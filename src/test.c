//
// Created by vad1mchk on 30.11.23.
//

#include <stdbool.h>
#include <stdio.h>
#include "test.h"

static void test_start_message(const char* test_name) {
    printf("Test \033[34m\"%s\"\033[0m started.\n", test_name);
}

static void test_end_message(const char* test_name, bool success) {
    printf(
        "Test \033[34m\"%s\"\033[0m finished (result: %s).\n", test_name,
        success ? "\033[32;1mSUCCESS\033[0m" : "\033[31;1mFAILURE\033[0m"
    );
}

int run_all_tests(void) {
    int result = 0;

    result |= test_allocation_successful();
    result |= test_freed_one_block();
    result |= test_freed_two_blocks();
    result |= test_new_region_extends_old();
    result |= test_new_region_allocated_elsewhere();

    puts("Test results:");
    for (int i = 0; i < 5; ++i) {
        bool failure = (result) & 1;
        result >>= 1;
        printf("\033[%s\033[0m ", failure ? "31;1mX" : "32;1mV");
    }
    return result;
}

int test_allocation_successful(void) {
    bool success = false;

    const char* test_name = "Successfully allocated";
    test_start_message(test_name);

    test_end_message(test_name, success);
    return (1 & !success);
}

int test_freed_one_block(void) {
    bool success = true;

    const char* test_name = "Freed 1 block";
    test_start_message(test_name);

    test_end_message(test_name, success);
    return (1 & !success) << 1;
}

int test_freed_two_blocks(void) {
    bool success = false;

    const char* test_name = "Freed 2 blocks";
    test_start_message(test_name);

    test_end_message(test_name, success);
    return (1 & !success) << 2;
}

int test_new_region_extends_old(void) {
    bool success;

    const char* test_name = "Out of memory, new region extends old";
    test_start_message(test_name);

    test_end_message(test_name, success);
    return (1 & !success) << 3;
}

int test_new_region_allocated_elsewhere(void) {
    bool success;

    const char* test_name = "Out of memory, new region allocated elsewhere";
    test_start_message(test_name);

    test_end_message(test_name, success);
    return (1 & !success) << 4;
}
