//
// Created by vad1mchk on 30.11.23.
//

#ifndef _TEST_H
#define _TEST_H

int run_all_tests(void);

int test_allocation_successful(void);

int test_freed_one_block(void);

int test_freed_two_blocks(void);

int test_new_region_extends_old(void);

int test_new_region_allocated_elsewhere(void);

#endif // _TEST_H
