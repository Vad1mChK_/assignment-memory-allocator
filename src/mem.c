#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ...);
void debug(const char* fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void* restrict addr, block_size block_sz, void* restrict next) {
    *((struct block_header*)addr) = (struct block_header) {
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region* r);

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap(
        (void*) addr,
        length,
        PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS | additional_flags,
        0,
        0
    );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region (void const* addr, size_t query) {
    size_t region_size = region_actual_size(query);
    void* region_address = map_pages(addr, region_size, 0);

    if (region_address == MAP_FAILED) {
        return REGION_INVALID;
    }

    block_init(addr, (block_size) { query }, NULL);

    struct region result = (struct region) { region_address, query, false };
    return result;
}

static void* block_after(struct block_header const* block);

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/* --- Разделение блоков (если найденный свободный блок слишком большой) --- */

static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free &&
        query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    size_t remaining_size = block->capacity.bytes - query - offsetof(struct block_header, contents);
    if (remaining_size >= BLOCK_MIN_CAPACITY && block_splittable(block, query)) {
        struct block_header* new_block = (struct block_header*) (
            (uint8_t*) block + query + offsetof(struct block_header, contents)
        );
        new_block->next = block->next;
        new_block->capacity.bytes = remaining_size;
        new_block->is_free = true;
        block->next = new_block;
        block->capacity.bytes = query;
        return true;
    }
    return false;
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after(struct block_header const* block) {
    return (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (struct block_header const* first, struct block_header const* second ) {
    return (void*) second == block_after(first);
}

static bool mergeable(struct block_header const* restrict first, struct block_header const* restrict second) {
    return first->is_free && second->is_free && blocks_continuous(first, second);
}

static bool try_merge_with_next(struct block_header* block) {
    struct block_header* next_block = block->next;
    if (block == NULL || next_block == NULL || !next_block->is_free || !mergeable(block, next_block)) {
        return false;
    }

    block->capacity.bytes += next_block->capacity.bytes + offsetof(struct block_header, contents);
    block->next = next_block->next;
    return true;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    struct block_header* current = block;
    struct block_header* last = NULL;

    while (current != NULL) {
        if (block_is_big_enough(sz, current) && current->is_free) {
            return (struct block_search_result) {
                BSR_FOUND_GOOD_BLOCK, current
            };
        }
    }

    return (struct block_search_result) {
        last != NULL ? BSR_REACHED_END_NOT_FOUND : BSR_CORRUPTED,
        last
    };
}

/*
 * Попробовать выделить память в куче начиная с блока `block`, не пытаясь расширить кучу.
 * Можно переиспользовать, как только кучу расширили.
 */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
    }
    return search_result;
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    const size_t actual_size = region_actual_size(query);
    void* region_address = map_pages(block_after(last), actual_size, 0);
    if (region_address == NULL) {
        return NULL;
    }

    block_init(region_address, (block_size) { actual_size }, NULL);
    return try_merge_with_next(last) ? last : region_address;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = try_memalloc_existing(query, heap_start);

    struct block_header* new_block = NULL;
    switch (search_result.type) {
        case BSR_REACHED_END_NOT_FOUND: {
            new_block = grow_heap(search_result.block, query);
            break;
        }
        case BSR_CORRUPTED: {
            new_block = heap_init(query);
            break;
        }
        default: {
            new_block = search_result.block;
        }
    }

    if (new_block == NULL) {
        return NULL;
    }

    split_if_too_big(new_block, query);
    new_block->is_free = false;
    return search_result.block;
}

void* _malloc(size_t query) {
    struct block_header* const addr = memalloc(query, (struct block_header*) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    /*  ??? */
}
